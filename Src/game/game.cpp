//第二次更新更新了怪物移动，但是速率有待改善
//第三次更新添加钥匙和颜色系统，以后可以自己调颜色了哦
//第四次更新开始界面延迟，和跳跃改善（我也不知道，是改好还是改坏），可以自己调节
//第五次重大更新！！！！！！！！！！将怪物等以后所有的数组化，可以自由添加怪物等，以及代码英文化
//第六次更新，对之前跳跃的修复，不会跳的时候变慢，以及为以后困难的会跳系统的助力
//第七次更新，增加平台和刺 ，超多BUG
//第八次重大更新，大型地图加存档点，倒计时6天！！！
//第九次更新，增加子弹系统，可以更改屏幕上最多出现的子弹数，超多BUG，感觉试玩调查 ,减少了一点了
//第十次更新，增加彩蛋和新手关卡 ,和子弹向下发射 ,和小BUG修复
//第十一次更新，增加boss随机乱动，但是需要大家的意见！
//第十二次更新，增加boss两个阶段，可以发射子弹打boss，以及碰到boss扣血！
//第十三次更新，增加boss两个阶段，增加了一阶段得boss和第一关的完善！
//第十四次更新，音乐以及转移编译器！
#include"pch.h"
using namespace std;

string start1[100] = {
	"|—————————————————————————————————————————————————————————————————————————————|\n"
	"|                                                          \n"
	"|                      ***                  ********                     *                      * * *                  ***********                         |\n"
	"|                    **                        **                       * *                     *     *                     **                             |\n"
	"|                   *                          **                      *   *                    *      *                    **                             |\n"
	"|                     **                       **                     *     *                   *     *                     **                             |\n"
	"|                        *                     **                    *       *                  * * *                       **                             |\n"
	"|                         **                   **                   * * * * * *                 *  *                        **                             |\n"
	"|                          *                   **                  *           *                *    *                      **                             |\n"
	"|                       **                     **                  *           *                *     *                     **                             |\n"
	"|                   ***                        **                  *           *                *      *                    **                             |\n"
	"|                                                          \n"
	"|—————————————————————————————————————————————————————————————————————————————|\n"
	" \n"
	"                                                                       操作说明\n"
	"                                                                       a 左移\n"
	"                                                                       d 右移\n"
	"                                                                       w 上跳\n"
	"                                                                       s 下移\n"
	"                                                             请注意躲避'<'、‘$’等攻击 空格攻击\n"
	"                                                                祝你有个愉快的游戏体验\n"
	"                                                                  请按空格键开始游戏"
};
string start3[100] = {
	"|—————————————————————————————————————————————————————————————————————————————|          \n"
	"|                                                                                                        \n"
	"|    o o o          o o o o                      o o o o        o                 o              o       o         |\n"
	"|         o       o        o                     o       o      o                o  o             o     o          |\n"
	"|          o     o          o                    o        o     o               o    o             o   o           |\n"
	"|          o     o          o                    o        o     o              o      o             o o            |\n"
	"|    o o o       o          o                    o o o o o      o             o o o o  o             o             |\n"
	"|         o      o          o                    o              o             o        o             o             |\n"
	"|          o     o       o  o                    o              o             o        o             o             |\n"
	"|          o      o        o                     o              o             o        o             o             |\n"
	"|   o o o o        o o o o   o                   o              o o o o o     o        o             o             |\n"
	"|                                                                                                                           \n"
	"|                                                                                                                                                                      \n"
	"|                       o                                                                                                                                               \n"
	"|                      o o                                                                                                                                               \n"
	"|                     o o o                                                                                                                                                 \n"
	"|                    o o o o                                                                                                                                                  \n"
	"|                 o o o o o o o                                                                                                                                               \n"
	"|               o o o o o o o o o                                                                                                                                               \n"
	"|             o o o o o o o o o o o                                                                                                                                               \n"
	"|                 o o o o o o o                                                                                                                                                               \n"
	"|                o o o o o o o o                                                                                              o                                                 \n"
	"|               o o o o o o o o o                                                                                            o o                                                  \n"
	"|              o o o o o o o o o o                                                                                          o o o                                                  \n"
	"|             o o o o o o o o o o o                                                                                        o o o o                                                                     \n"
	"|            o o o o o o o o o o o o                                                                                         o o                                                   \n"
	"|        o o o o o o o o o o o o o o o o                                                                                    o o o                                                         \n"
	"|       o o o o o o o o o o o o o o o o o o                                                                                o o o o                                                           \n"
	"|      o o o o o o o o o o o o o o o o o o o                              o                                             o o o o o o o                                              \n"
	"|   o o o o o  o o o o o o o o o o o o o o o o                           o o                                           o o o o o o o o                                             \n"
	"|            o o o o o o o o o o o o                                    o o o                                         o o o o o o o o o                                                     \n"
	"|          o o o o o o o o o o o o o o                                 o o o o                                       o o o o o o o o o o                                                       \n"
	"|         o o o o o o o o o o o o o o o o                           o o o o o o o                                 o o o o o o o o o o o o o                                                               \n"
	"|       o o o o o o o o o o o o o o o o o o                        o o o o o o o o                              o o o o o o o o o o o o o o o                                                                   \n"
	"|      o o o o o o o o o o o o o o o o o o o                      o o o o o o o o o                                         o o o o                                                                      \n"
	"|                   o o o o o o                                  o o o o o o o o o o                                       o o o o o                                                        \n"
	"|                   o o o o o o                                       o o o o o                                           o o o o o o                                                       \n"
	"|                   o o o o o o                                      o o o o o o                                         o o o o o o o                                                        \n"
	"|                   o o o o o o                                     o o o o o o o                                              o                                                         \n"
	"|                   o o o o o o               o                   o o o o o o o o o                      o                     o                                                           \n"
	"|                   o o o o o o              o o                 o o o o o o o o o o                    o o                    o                                                              \n"
	"|                   o o o o o o             o o o               o o o o o o o o o o o                  o o o                   o                                                               \n"
	"|                   o o o o o o            o o o o                      o o o                            o                     o                                                     \n"
	"|                   o o o o o o               o                         o o o                            o                     o                                                     \n"
	"|—————————————————————————————————————————————————————————————————————————————|\n" };
string start2[100] = {
	"|————————————————————————————————————————————|\n"
	"|                                                                                        |\n"
	"|            * * * *        *  * *           *  * *          * * * *                     |\n"
	"|          *              *        *       *        *        *       *                   |\n"
	"|        *               *          *     *          *       *         *                 |\n"
	"|       *                *          *     *          *       *          *                |\n"
	"|       *          *     *          *     *          *       *          *                |\n"
	"|       *          *     *          *     *          *       *         *                 |\n"
	"|        *         *     *          *     *          *       *        *                  |\n"
	"|         *        *       *       *        *       *        *       *                   |\n"
	"|           * * * **         * * *            * * *          * * * *                     |\n"
	"|                                                                                        |\n"
	"|                                                                                        |\n"
	"|        * *        *      * * * * * *     *         *       * * * * * * *               |\n"
	"|        *   *      *      *                *       *              *                     |\n"
	"|        *    *     *      *                 *     *               *                     |\n"
	"|        *     *    *      *                  *   *                *                     |\n"
	"|        *      *   *      * * * * * *         * *                 *                     |\n"
	"|        *       *  *      *                    *                  *                     |\n"
	"|        *        * *      *                   * *                 *                     |\n"
	"|        *         *       *                  *   *                *                     |\n"
	"|        *          *      * * * * * *       *     *               *                     |\n"
	"|————————————————————————————————————————————|\n"
	
};
string bossmap[101] = {
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"                                                                                                                                        ",
	"########### ##################### ######################################## ################ ################################     ",
	"########### ##################### ######################################## ################ #################################     ",
	"########################################################################################### ##################################       ",

};
string boss1[101] = {
		   "  *  * * *            ",
		   " *  @ @     *         ",
		   "*   ''       *  *  *  ",
		   "  *  + +     *  *  *  *",
		   "   *  *  *          * ",
};
string boss2[101] = {
			 "   * *  *  *   ",
			 " *   @   @   * ",
			 "*      ;      *",
			 " *   +++++   * ",
			 "   *  *  *  *  ",
			 "     ||  ||    ",
			 "     //   \\   ",
			 "    //     \\  ",

};



string map1[100] = {
	"*         **                                                                                   oooo|                   ",
	"*         **                                                                                     oo|                   ",
	"*         **                    ** *** **    ** **** *                                            o|                  ",
	"***** *   **                    *   *   *   **   *                     *    ***********************|********        ",
	"*         **        o           *   *   *  * *   ** **                **    **                         _*      ",
	"*    !    **       ooo          * 2 *   * *  *   *                    **    **     *               *  |_|     ",
	"**** * *****      ooooo         *****   *    *   *****                **    **  ******************** ********      ",
	"          **        o       *                              *    R     **                   **               *  ",
	" !!       **       *************************************************  **    ******    *******************   *  ",
	"*******  ***     ****                *****                            **   ***                          *   *  ",
	"*         **                         **                             ****    **                *  R      **  *  ",
	"*         **                     * K **              *******        ***  ** ************************    *   *  ",
	"*         **                     ******     *                       ***                       **        *  **  ",
	"*         **                  *********   ******************************   *                  **            *  ",
	"*   !!!!!!**                        ***                               **                      **    ******  *  ",
	"    ********     R *                *****                             **     ****      ****   **        *   *  ",
	"          **  ***************************   *                    *    **                                *  **  ",
	"          **               *     **********************************   **          ****     **********   *   *  ",
	"          ***                        **                               **                      **      ****  *  ",
	"5         ***                        **                               **             **                     *  ",
	"********  ***  *               *     **                          * R  **                      **    **  *   *  ",
	"            **********************   **     ***************************   !!!!!!!!!!!!!!!!!!! **        *  **  ",
	"            **                 **    *****                           **  ***********************       **   *  ",
	"        **                           ***                             *                                  *   *  ",
	"                                   ****                           *R *                                ****  *   ",   
	"                                   ****                           ** *              **                ***   *  ",
	"     R       *                 *   ****  *!!**!!!!!!!!!!!!!!!!!!!!**     !!!!!!!!!!!**!!!!!!!!!!!!!!!!***!! *  ",
	" *************************************  *********************************************************************                                ",
	" ********* ******** ********** *******   ********************************************************************                                   ",
	"                                       *  R      0***********************************************************                                  ",
	"*************************************************************************************************************                                   ",
	"0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678 ",
};

int h = 28, w = 105, q = 1,
key, monsternumber = 10,
bulletnumber = 20, trapnumber = 30,
floodnumber = 5, keepx = 0, keepy = 1,
shotnumber = 6, finaldirection = 1,
p = 0, pass = 0, win = 0,x2=5,y2=34;
int bossuserx = 10, bossusery = 10, whichboss, bossbulletnumber = 100, missilenumber = 10, pointnumber = 5;
int magic[20];
struct node {
	int x, y, direction, Xinitial, Yinitial, flag, towards;
};
struct bos {
	int x, y, bh, bw, hp;
};

node bullet[140] = { {0,0,1,26,30,0,1},{0,0,-1,15,19,0,-1},{0,0,1,15,36,0,1},{0,0,-1,9,20,0,-1},
{0,0,-1,9,42,0,-1},{0,0,1,12,68,0,1},{0,0,1,25,65,0,1},{0,0,1,23,68,0,1},{0,0,-1,20,39,0,-1},{0,0,1,29,39,0,1}
,{0,0,1,16,65,0,1},{0,0,1,19,105,0,1 },{0,0,1,13,105,0,1 },{0,0,1,24,100,0,1},{0,0,-1,16,72,0,-1},{0,0,-1,14,72,0,-1} };

node monster[140] = { {0,0,1,20,20},{0,0,-1,12,45},{0,0,-1,16,49},{0,0,-1,8,1},{0,0,1,7,30},{0,0,1,29,30},{0,0,1,10,78},{0,0,-1,10,93},{0,0,1,5,98} };

node trap[140] = { {0,0,1,27,14},{0,0,1,27,15},{0,0,1,27,25},{0,0,1,21,21},{0,0,1,21,22},{0,0,1,17,46},
{0,0,1,17,47},{0,0,1,17,57},{0,0,1,17,58},{0,0,-1,15,5},{0,0,-1,15,6},{0,0,1,21,60},{0,0,1,21,59},{0,0,1,21,48},
{0,0,1,21,47},{0,0,1,8,27},{0,0,-1,14,48},{0,0,-1,14,64},{0,0,-1,18,55},{0,0,-1,18,54},{0,0,1,8,26},{0,0,1,8,25},{0,0,1,11,84},{0,0,1,6,82},{0,0,1,27,107} };
node user = { 0,0,1,1,1,1 };
node flood[140] = { {0,0,1,11,18,0,4},{0,0,1,11,3,0,3},{0,0,1,24,42,0,4},{0,0,1,25,71,0,3},{0,0,1,25,87,0,3} };//toward 此时是平台长度//x,y是平台最左边的位置;
node shot[140];
bos boss[2] = { {22,45,5,22,1},{19,45,8,15,10} };
node bossbullet[300];
node bossmissile[300];
node hand[10];
node point[10];
node protect[10];
void hide() {
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO CursorInfo;
	GetConsoleCursorInfo(handle, &CursorInfo);//获取控制台光标信息
	CursorInfo.bVisible = false; //隐藏控制台光标
	SetConsoleCursorInfo(handle, &CursorInfo);//设置控制台光标状态
}
void gotoxy(int x, int y) {
	hide();
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;
	pos.X = x;
	pos.Y = y;
	SetConsoleCursorPosition(handle, pos);
}
int bulletcheck(int x, int y) {
	for (int i = 0; i < bulletnumber; i++)
		if (x == bullet[i].x&&bullet[i].y == y)
			return bullet[i].towards;
	return 0;
}
int trapcheck(int x, int y) {
	for (int i = 0; i < trapnumber; i++)
		if (x == trap[i].x&&trap[i].y == y)
			return 1;
	return 0;
}
int monstercheck(int x, int y) {
	for (int i = 0; i < monsternumber; i++)
		if (x == monster[i].x&&monster[i].y == y)
			return 1;
	return 0;
}
int shotcheck(int x, int y) {
	for (int i = 0; i < shotnumber; i++)
		if (x == shot[i].x&&shot[i].y == y && shot[i].flag)
			return shot[i].direction;
	return 0;
}
int floodcheck(int x, int y) {
	for (int i = 0; i < floodnumber; i++)
		if (x == flood[i].x&&flood[i].y <= y && flood[i].y + flood[i].towards - 1 >= y)
			return 1;
	return 0;
}
void start() {
	for (int i = 0; i < 13; i++)
		for (int j = 0; j < start1[i].size(); j++)
			Sleep(2), printf("%c", start1[i][j]);

}
void bulletover(int cnt) {
	bullet[cnt].x = bullet[cnt].Xinitial;
	bullet[cnt].y = bullet[cnt].Yinitial;
}
void monsterover(int cnt) {
	monster[cnt].x = monster[cnt].Xinitial;
	monster[cnt].y = monster[cnt].Yinitial;
}
void trapover(int cnt) {
	trap[cnt].x = trap[cnt].Xinitial;
	trap[cnt].y = trap[cnt].Yinitial;
	trap[cnt].flag = 0;
}
void floodover(int cnt) {
	flood[cnt].x = flood[cnt].Xinitial;
	flood[cnt].y = flood[cnt].Yinitial;
}
void shotover(int cnt,int Q) {
	shot[cnt].x = user.x+Q;
	shot[cnt].y = user.y +  finaldirection;
	shot[cnt].flag = 1;
	shot[cnt].direction = finaldirection;
	shot[cnt].towards = Q;
}
void startover() {
	user.x = keepx;
	user.y = keepy;
	
	Sleep(500);
	for (int i = 0; i < bulletnumber; i++)
		bulletover(i);
	for (int i = 0; i < monsternumber; i++)
		monsterover(i);
	for (int i = 0; i < trapnumber; i++)
		trapover(i);
	for (int i = 0; i < floodnumber; i++)
		floodover(i);

}
void  bulletupdate(int cnt) {
	if (bullet[cnt].y <= 0 || bullet[cnt].y >= w || map1[bullet[cnt].x][bullet[cnt].y - bullet[cnt].direction] == '*')
		bulletover(cnt);
	bullet[cnt].y -= bullet[cnt].direction;
	if (bullet[cnt].x == user.x && bullet[cnt].y == user.y)
		startover();
}
int  bosscheck(int x, int y) {
	if (boss[whichboss].x <= x && x < boss[whichboss].x + boss[whichboss].bh&&boss[whichboss].y <= y && y < boss[whichboss].y + boss[whichboss].bw)
		return 1;
	return 0;
}
void  trapupdate(int cnt) {

	if (user.y == trap[cnt].y) {
		if (trap[cnt].direction == 1 && trap[cnt].x > user.x&&user.x>=trap[cnt].x-6)
			trap[cnt].flag = 1;
		if (trap[cnt].direction == -1 && trap[cnt].x < user.x&&user.x <= trap[cnt].x + 8)
			trap[cnt].flag = 1;
	}
	if (trap[cnt].x == user.x &&trap[cnt].y == user.y)
		startover();
	if (trap[cnt].flag) trap[cnt].x -= trap[cnt].direction;
	if (trap[cnt].x == user.x &&trap[cnt].y == user.y)
		startover();

}
void  monsterupdate(int cnt) {
	if (monster[cnt].y - monster[cnt].direction < 0 || map1[monster[cnt].x][monster[cnt].y - monster[cnt].direction] == '*' || monster[cnt].y == 0)
		monster[cnt].direction *= -1;
	monster[cnt].y -= monster[cnt].direction;
	if (monster[cnt].x == user.x && monster[cnt].y == user.y)
		startover();
	//if (monster[cnt].x == (user.x+1) && monster[cnt].y == user.y)
	//monster[cnt].y=-1,monster[cnt].x=-1;
}
void  floodupdate(int cnt) {
	if (map1[flood[cnt].x][flood[cnt].y + flood[cnt].towards] == '*' || map1[flood[cnt].x][flood[cnt].y - 1] == '*' || flood[cnt].y <= 0)
		flood[cnt].direction *= -1;
	if (flood[cnt].x == user.x + 1 && flood[cnt].y <= user.y&&flood[cnt].y + flood[cnt].towards - 1 >= user.y)
		user.y += flood[cnt].direction;
	flood[cnt].y += flood[cnt].direction;

	//if (monster[cnt].x == (user.x+1) && monster[cnt].y == user.y)
}

void elseupdate() {
	for (int i = 0; i < bulletnumber; i++)
		bulletupdate(i);
	for (int i = 0; i < monsternumber; i++)
		monsterupdate(i);
	for (int i = 0; i < trapnumber; i++)
		trapupdate(i);
	for (int i = 0; i < floodnumber; i++)
		floodupdate(i);
	
	if (user.y == 0)
		q = 0;
	if (user.x >= 28)
		h = 31;
	if (user.y >=105)
		w=109;
	if (map1[user.x][user.y + 1] == '|'&&map1[user.x][user.y - 1] == '|'&& key)
		pass = 1;
	if (map1[user.x][user.y] <= '9' && map1[user.x][user.y] >= '0')
		magic[map1[user.x][user.y] - '0'] = 1;
	if (map1[user.x][user.y] == 'K')
		key = 1;
	if (map1[user.x][user.y] == 'G'&&key==1)
		pass = 1;
	if (map1[user.x][user.y] == '!')
		startover();
	if (map1[user.x][user.y] == 'R')
		keepx = user.x, keepy = user.y;

}
void show() {

	gotoxy(0, 0);
	for (int i = 0; i < h; i++) {
		for (int j = q; j < w; j++) {
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);//设置颜色
			if (i == user.x&&j == user.y) {
				if (key == 1) {
					SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED);//设置颜色
					putchar('H');
				}
				else
					putchar('H');
			}
			else if (bulletcheck(i, j) == 1)
				putchar('<');
			else if (bulletcheck(i, j) == -1)
				putchar('>');
			else if (shotcheck(i, j) == 1)
				putchar(')');
			else if (shotcheck(i, j) == -1)
				putchar('(');
			else if (floodcheck(i, j))
				putchar('#');
			else if (monstercheck(i, j))
				putchar('$');
			else if (trapcheck(i, j))
				putchar('^');
			else
			{
				if (i == x2 && j == y2 && (user.x >= x2 || user.y <= y2 - 6 || user.y >= y2 + 6))
					putchar(' ');
				else	
					putchar(map1[i][j]);
			}
			
		}
		putchar('\n');
	}
	elseupdate();
	printf("按Q重新开始 ");
	if(magic[5]==1)
		printf("特殊子弹 ");
	if (magic[2] == 1)
		printf("生命 ++ ");
	if (magic[0] == 1)
		printf(" Boss关跳跃增强 " );
}
void fight() {
	shotover(p,0);
	p++;
	p %= shotnumber - 1;
	if (magic[5] == 1)
	{
		shotover(p,1);
		p++;
		p %= shotnumber - 1;
		shotover(p,-1);
		p++;
		p %= shotnumber - 1;
	}
}
bool check(int x, int y) {
	if (x >= 0 && y >= 0 && !floodcheck(x, y) && (map1[x][y] != '*'&&map1[x][y] != '#'))
		return 1;
	return 0;
}
bool check1(int x, int y) {
	if (x >= 0 && y >= 0 && (map1[x][y] != '*'&&map1[x][y] != '#'))
		return 1;
	return 0;
}
void up() {
	bool flag = 1;
	for (int i = 1; i <= 5; i++) {
		if ((i == 1 || i == 2 || i == 3 || i == 5) && check(user.x - 1, user.y))
			user.x--;
		if (_kbhit() && flag&&i >= 2) {
			flag = 0;
			char input = _getch();
			if (input == ' ')
				fight();
			if (input == 'a'&&check(user.x, user.y - 1) && check(user.x, user.y - 2))
				user.y--, show(), user.y--;
			else if (input == 'a'&&check(user.x, user.y - 1))
				user.y--, show();
			if (input == 'd'&&check(user.x, user.y + 1) && check(user.x, user.y + 2))
				user.y++, show(), user.y++;
			else if (input == 'd'&&check(user.x, user.y + 1))
				user.y++, show();
			break;
		}
		show();

	}


}
void down() {
	bool flag = 1;
	int i;
	for (i = 0; check(user.x + 1, user.y) && i < 100; i++)
		if ((i == 2 || i == 3) || i == 4 || i == 5 || i > 5) {
			user.x++;
			if (flag && _kbhit()) {
				flag = 0;
				char input = _getch();
				if (input == ' ')
					fight();
				if (input == 'a'&&check(user.x, user.y - 1) && check(user.x, user.y - 2))
					user.y--, show(), user.y--;
				else if (input == 'a'&&check(user.x, user.y - 1))
					user.y--, show();
				if (input == 'd'&&check(user.x, user.y + 1) && check(user.x, user.y + 2))
					user.y++, show(), user.y++;
				else if (input == 'd'&&check(user.x, user.y + 1))
					user.y++, show();
			}
			show();
		}

}
void did() {
	char input;
	if (_kbhit()) {
		input = _getch();
		if (input == 'q')
			startover();
		if (input == 'a'&&check(user.x, user.y - 1))
			user.y--, finaldirection = -1;
		if (input == 'd'&&check(user.x, user.y + 1))
			user.y++, finaldirection = 1;
		if (input == 'w')
			up();
		if (input == 'p')
			pass=1;
		//if(input==' ')
			//fight();
		//if (input == ' '&&t>0)
		//for(;t>0;t--)
		//did(),Sleep(500),show();
		down();
	}
}
int othercheck(int x, int y)
{
	if (x >= 0 && y >= 0 && y <= w && x <= h)
		return 1;
	return 0;
}
int bossbulletcheck(int x, int y) {
	for (int i = 0; i < bossbulletnumber; i++)
		if (x == bossbullet[i].x&&bossbullet[i].y == y && bossbullet[i].flag == 1)
			return 1;
	return 0;
}
int bossmissilecheck(int x, int y) {
	for (int i = 0; i < missilenumber; i++)
		if (x == bossmissile[i].x&&bossmissile[i].y == y && bossmissile[i].flag == 1)
			return bossmissile[i].Yinitial;
	return 0;
}
int protectcheck(int x, int y) {
	for (int i = 0; i < 1; i++)
		if (y == protect[i].y&&protect[i].x <=x && protect[i].x+ protect[i].Yinitial>= x&& protect[i].flag == 1)
			return 1;
	return 0;
}
int pointcheck1(int x, int y) {
	for (int i = 0; i < pointnumber; i++)
		if (x == point[i].x&& point[i].y == y && point[i].flag == 1)
			return  1;
	return 0;
}
int pointcheck2(int x, int y) {
	for (int i = 0; i < pointnumber; i++)
		if ((x == point[i].x || point[i].y == y) && point[i].flag == 1 && point[i].Xinitial == 1)
			return  1;
	return 0;
}
void bossuserover() {
	user.x = bossuserx;
	user.y = bossusery;
	user.flag = 3+magic[2]*2;
}
void bossbulletvoer() {
	for (int i = 0; i < bossbulletnumber; i++)
		bossbullet[i].flag = 0;
}
void bossmissilevoer() {
	for (int i = 0; i < missilenumber; i++)
		bossmissile[i].flag = 0;
}
void pointover() {
	for (int i = 0; i < pointnumber; i++)
		point[i].flag = 0;
}
void gameover() {
	bossuserover();
	bossbulletvoer();
	bossmissilevoer();
	pointover();
	Sleep(1000);
	whichboss = 0;
	bossbulletnumber = 100;
	missilenumber = 7;
	pointnumber = 5;
	boss[whichboss].x = 22;
	boss[whichboss].y = 45;
	boss[0].hp = 20;
	boss[1].hp = 30;
	mciSendString(TEXT("close MySong2"), NULL, 0, NULL);
	mciSendString(TEXT("close MySong1"), NULL, 0, NULL);
	mciSendString(TEXT("open boss1.wav alias mysong1"), NULL, 0, NULL);
	mciSendString(TEXT("play MySong1"), NULL, 0, NULL);
}
void producepoint(int x)
{
	point[x].x = rand() % 20 + 4;
	point[x].y = rand() % 50 + 10;
	point[x].flag = 1;
	point[x].Xinitial = 0;
	point[x].towards = 15;
}
void producemissile(int x) {
	bossmissile[x].flag = 1;
	bossmissile[x].towards = rand() % 5 - 2;
	bossmissile[x].x = rand() % 20 + 1;
	if (bossmissile[x].towards > 0)
		bossmissile[x].Yinitial = 1, bossmissile[x].y = 0;
	else if (bossmissile[x].towards < 0)
		bossmissile[x].Yinitial = -1, bossmissile[x].y = w;
	else
		bossmissile[x].flag = 0;
}
void producebullet(int x) {
	bossbullet[x].flag = 1;
	bossbullet[x].direction = rand() % 3 - 1;
	bossbullet[x].towards = rand() % 3 - 1;
	if (bossbullet[x].direction == 0 && bossbullet[x].towards == 0)
		bossbullet[x].flag = 0;
	bossbullet[x].x = boss[whichboss].x + rand() % 10;
	bossbullet[x].y = boss[whichboss].y + rand() % 10;
}
void bossstep1() {
	int q = rand() % 3;
	int step = rand() % 3 * 2 + 1;
	if (q == 1 && boss[whichboss].y + boss[whichboss].bw + step < w) boss[whichboss].y += step;
	if (q == 0 && boss[whichboss].y - step >= 0) boss[whichboss].y -= step;
}
void bossstep2() {
	int q = rand() % 3;
	int step = rand()%5+1;
	if (q == 1 && boss[whichboss].x + boss[whichboss].bh + step < h -10 ) boss[whichboss].x += step;
	if (q == 0 &&  boss[whichboss].x - step >= 0) boss[whichboss].x -= step;
}
void  shotupdate(int cnt) {
	if (shot[cnt].x + shot[cnt].towards <= 0 || shot[cnt].y + shot[cnt].direction <= 0 || shot[cnt].y + shot[cnt].direction >= w || map1[shot[cnt].x + shot[cnt].towards][shot[cnt].y + shot[cnt].direction] == '*')
		shot[cnt].flag = 0;
	//if (map1[shot[cnt].x][shot[cnt].y-shot[cnt].direction]=='*'||shot[cnt].y<=0||shot[cnt].y>=w)
		//shot[cnt].flag=0;
	if (bosscheck(shot[cnt].x, shot[cnt].y) && shot[cnt].flag)
	{
		if (whichboss ==0 && boss1[shot[cnt].x - boss[whichboss].x][shot[cnt].y - boss[whichboss].y] == '*')
			boss[whichboss].hp--, shot[cnt].flag = 0;
		if (whichboss == 1 && boss2[shot[cnt].x - boss[whichboss].x][shot[cnt].y - boss[whichboss].y]=='*')
				boss[whichboss].hp--, shot[cnt].flag = 0;
	}
		
	if (shot[cnt].flag)
	{
		shot[cnt].y += 2 * shot[cnt].direction;
		shot[cnt].x += shot[cnt].towards;
	}
	if (protectcheck(shot[cnt].x, shot[cnt].y))
		shot[cnt].flag = 0;

}
void bossmissileupdate(int x)
{
	if (!othercheck(bossmissile[x].x, bossmissile[x].y))
		bossmissile[x].flag = 0;
	if (shotcheck(bossmissile[x].x, bossmissile[x].y))
		bossmissile[x].flag = 0;
	if (bossmissile[x].x == user.x&&bossmissile[x].y == user.y)
		bossmissile[x].flag = 0, user.flag--;
	bossmissile[x].y += bossmissile[x].towards;
}
void bossbulletupdate(int x)
{
	if (!othercheck(bossbullet[x].x, bossbullet[x].y))
		bossbullet[x].flag = 0;
	if (bossbullet[x].x == user.x&&bossbullet[x].y == user.y)
		bossbullet[x].flag = 0, user.flag--;
	bossbullet[x].x += bossbullet[x].direction;
	bossbullet[x].y += bossbullet[x].towards;
	if (!othercheck(bossbullet[x].x, bossbullet[x].y) || map1[bossbullet[x].x][bossbullet[x].y] == '#')
		bossbullet[x].flag = 0;
}
void pointupdate(int x)
{
	if (point[x].Xinitial == 1 && (user.x == point[x].x || user.y == point[x].y))
		user.flag--;
	point[x].towards -= rand() % 3;
	if (point[x].towards <= 0)
		point[x].Xinitial = 1;
	if (point[x].towards <= -20)
		point[x].flag = 0;
}
void protectupdate(int x)
{
	protect[x].towards -= rand() % 3;
	if (protect[x].towards <= 0)
		protect[x].flag = 0;
}
void updateinboss() {
	int done = rand() % 20;
	if (done <= 4)
		bossstep1();
	if (done <= 3 && whichboss == 1)
		bossstep2();
	if (done == 10 )
	{
		for (int i = 0; i < pointnumber; i++)
			if (rand() % 3 == 0 && point[i].flag == 0)
				producepoint(i);
	}
	if (done == 6)
	{
		for (int i = 0; i < bossbulletnumber; i++)
			if (bossbullet[i].flag == 0)
				producebullet(i);
	}
	if (done == 7)
	{
		for (int i = 0; i < missilenumber; i++)
			if (bossmissile[i].flag == 0)
				producemissile(i);
	}
	if (done == 13 && whichboss == 1)
	{
		if (protect[0].flag == 0)
		{
			protect[0].x = boss[whichboss].x;
			protect[0].y = boss[whichboss].y-3;
			protect[0].Yinitial = rand() % 3 + 5;
			protect[0].towards = 30;
			protect[0].flag = 1;
		}
	}	
	for (int i = 0; i < 1; i++)
		if (protect[i].flag)
			protectupdate(i);
	for (int i = 0; i < bossbulletnumber; i++)
		if (bossbullet[i].flag)
			bossbulletupdate(i);
	for (int i = 0; i < missilenumber; i++)
		if (bossmissile[i].flag)
			bossmissileupdate(i);
	for (int i = 0; i < pointnumber; i++)
		if (point[i].flag)
			pointupdate(i);

}
void bossshow() {
	gotoxy(0, 0);
	for (int i = 0; i < h + 2; i++) {
		for (int j = 0; j < w; j++) {
			if (i == user.x&&j == user.y) {
				putchar('H');
			}

			else if (bosscheck(i, j) == 1)
			{
				if (whichboss == 0)
					putchar(boss1[i - boss[whichboss].x][j - boss[whichboss].y]);
				if (whichboss == 1)
					putchar(boss2[i - boss[whichboss].x][j - boss[whichboss].y]);
			}
			else if (pointcheck1(i, j))
			{
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_BLUE);
				putchar('X');
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			}
			else if (pointcheck2(i, j)) {
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED);
				putchar('0');
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			}
			else if (protectcheck(i, j) == 1)
				putchar('|');
			else if (bossbulletcheck(i, j) == 1)
				putchar('@');
			else if (bossmissilecheck(i, j) == -1)
				putchar('<');
			else if (bossmissilecheck(i, j) == 1)
				putchar('>');
			else if (shotcheck(i, j) == 1)
				putchar(')');
			else if (shotcheck(i, j) == -1)
				putchar('(');

			else
				putchar(map1[i][j]);
		}
		putchar('\n');
	}
	printf("血量：%d", user.flag);
}
void bossdid() {
	char input;
	if (_kbhit()) {
		input = _getch();
		//	if (input == 'q')
		//		startover();
		if (input == 'a'&&check1(user.x, user.y - 1))
			user.y--, finaldirection = -1;
		if (input == 'd'&&check1(user.x, user.y + 1))
			user.y++, finaldirection = 1;
		if (input == 'w'&&check1(user.x-1, user.y - 1))
			user.x-=magic[0]+1;
		if (input == 's'&&check1(user.x + 1, user.y - 1))
			user.x+= magic[0] + 1;
		if (input == ' ')
			fight();
		//if (input == ' '&&t>0)
		//for(;t>0;t--)
		//did(),Sleep(500),show();
		//down();
	}
}
void bossupdate() {
	for (int i = 0; i < shotnumber; i++)
		shotupdate(i);
	updateinboss();
	if (boss[whichboss].hp <= 0)
	{
		if(whichboss==1)
		{
			win = 1;
			return;
		}
		Sleep(500);
		whichboss++;
		boss[whichboss].x = boss[whichboss - 1].x - 3;
		boss[whichboss].y = boss[whichboss - 1].y;
		bossbulletnumber = 150;
		missilenumber = 15;
		pointnumber = 8;
		
		mciSendString(TEXT("close MySong1"), NULL, 0, NULL);
		mciSendString(TEXT("open boss2.mp3 alias mysong2"), NULL, 0, NULL);
		mciSendString(TEXT("play MySong2"), NULL, 0, NULL);

	}
	if (bosscheck(user.x, user.y))
	{
		if (whichboss == 0 && boss1[user.x - boss[whichboss].x][user.y - boss[whichboss].y] != ' ')
			user.flag--;
		if (whichboss == 1 && boss2[user.x - boss[whichboss].x][user.y - boss[whichboss].y] != ' ')
			user.flag--;
	}
	if (user.flag <= 0)
		gameover();
}
int main() {
	
	int fps = 288;
	system("mode con cols=170 lines=40");
	srand((unsigned)time(0));
	
	start();
	char input = _getch();
	if(input == ' ')
	{
		mciSendString(TEXT("open 1.wav alias mysong2"), NULL, 0, NULL);
		mciSendString(TEXT("play MySong2"), NULL, 0, NULL);
		system("cls");
		startover();
		while(!pass) {
			Sleep(1000/fps);
			show();
			did();
			down();
		}
		system("cls");
		for (int i = 0; i < 44; i++)
			for (int j = 0; j < start2[i].size(); j++)
				putchar(start2[i][j]);
			}
		Sleep(5000);
		system("cls");
		mciSendString(TEXT(" 关卡 MySong2"), NULL, 0, NULL);
		shotnumber += magic[5] * 3;
		for (int i = 0; i < h + 2; i++)
			for (int j = 0; j < w; j++)
				map1[i][j] = bossmap[i][j];
			gameover();
		while (!win) {
			Sleep(1000 / fps);
			bossshow();
			bossupdate();
			bossdid();
		}
		system("cls");
		system("mode con cols=250 lines=100");
		for (int i = 0; i < 44; i++)
		for (int j = 0; j < start2[i].size(); j++)
			putchar(start3[i][j]);
		
	
	}
	
